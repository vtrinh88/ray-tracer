//
//  Vector.h
//  Ray Tracer
//
//  Created by Viet Trinh on 4/12/13.
//  Copyright (c) 2013 Viet Trinh. All rights reserved.
//

#ifndef Ray_Tracer_Vector_h
#define Ray_Tracer_Vector_h

class Vector{
public:
    float x;
    float y;
    float z;
    
    Vector(){x = 0.0;y=0.0;z=0.0;}
    Vector(float xp,float yp,float zp){x = xp;y=yp;z=zp;}
    
    Vector add(Vector* v);
    Vector subtract(Vector* v);
    Vector multiply(Vector* v);
    Vector crossProduct(Vector* v);
    float dotProduct(Vector* v);
    Vector multipleScalar(float coef);
    void normalize();
    float length();

};

#endif
