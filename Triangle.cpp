//
//  Triangle.cpp
//  Ray Tracer
//
//  Created by Viet Trinh on 4/13/13.
//  Copyright (c) 2013 Viet Trinh. All rights reserved.
//

#include "Triangle.h"

Triangle::Triangle(){
    v0 = new Vector();
    v1 = new Vector();
    v2 = new Vector();
    materialColor_amb = new Vector();
    materialColor_dif = new Vector();
    materialColor_spec = new Vector();
}

Triangle::Triangle(Vector* p0,Vector* p1,Vector* p2){
    v0 = p0;
    v1 = p1;
    v2 = p2;
    materialColor_amb = new Vector();
    materialColor_dif = new Vector();
    materialColor_spec = new Vector();
}

Triangle::Triangle(float v0x,float v0y,float v0z,
                   float v1x,float v1y,float v1z,
                   float v2x,float v2y,float v2z){
    v0 = new Vector(v0x,v0y,v0z);
    v1 = new Vector(v1x,v1y,v1z);
    v2 = new Vector(v2x,v2y,v2z);
    materialColor_amb = new Vector();
    materialColor_dif = new Vector();
    materialColor_spec = new Vector();
}

void Triangle::setTriangleColor(float aR,float aG,float aB,
                            float dR,float dG,float dB,
                            float sR,float sG,float sB){
    
    materialColor_amb->x = aR;
    materialColor_amb->y = aG;
    materialColor_amb->z = aB;
    
    materialColor_dif->x = dR;
    materialColor_dif->y = dG;
    materialColor_dif->z = dB;
    
    materialColor_spec->x = sR;
    materialColor_spec->y = sG;
    materialColor_spec->z = sB;
    
}

Vector Triangle::findPlaneNormal(){
    Vector normal;
    Vector e0 = v1->subtract(v0);
    Vector e1 = v2->subtract(v1);
    normal = e0.crossProduct(&e1);
    normal.normalize();
    return normal;
}

bool Triangle::isPointInTriangle(Vector* R, Vector* n){
    bool isIn = false;
    float term1,term2,term3;
    
    Vector e0 = v1->subtract(v0);
    Vector e1 = v2->subtract(v1);
    Vector e2 = v0->subtract(v2);
    
    Vector V0R = R->subtract(v0);
    Vector V1R = R->subtract(v1);
    Vector V2R = R->subtract(v2);
    
    term1 = e0.crossProduct(&V0R).dotProduct(n);
    term2 = e1.crossProduct(&V1R).dotProduct(n);
    term3 = e2.crossProduct(&V2R).dotProduct(n);
    
    if (term1>0 && term2>0 && term3>0) {
        isIn = true;
    }
    
    return isIn;
}

