//
//  Sphere.cpp
//  Ray Tracer
//
//  Created by Viet Trinh on 4/14/13.
//  Copyright (c) 2013 Viet Trinh. All rights reserved.
//

#include "Sphere.h"


Sphere::Sphere(){
    center = new Vector();
    radius=0.0;
    materialColor_amb = new Vector();
    materialColor_dif = new Vector();
    materialColor_spec = new Vector();
}

Sphere::Sphere(Vector* c,float r){
    center = c;
    radius=r;
    materialColor_amb = new Vector();
    materialColor_dif = new Vector();
    materialColor_spec = new Vector();
}

Sphere::Sphere(float cx,float cy,float cz,float r){
    center = new Vector(cx,cy,cz);
    radius=r;
    materialColor_amb = new Vector();
    materialColor_dif = new Vector();
    materialColor_spec = new Vector();
}

void Sphere::setSphereColor(float aR,float aG,float aB,
                    float dR,float dG,float dB,
                    float sR,float sG,float sB){
    
    materialColor_amb->x = aR;
    materialColor_amb->y = aG;
    materialColor_amb->z = aB;
    
    materialColor_dif->x = dR;
    materialColor_dif->y = dG;
    materialColor_dif->z = dB;
    
    materialColor_spec->x = sR;
    materialColor_spec->y = sG;
    materialColor_spec->z = sB;
    
}