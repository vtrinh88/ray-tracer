//
//  Sphere.h
//  Ray Tracer
//
//  Created by Viet Trinh on 4/13/13.
//  Copyright (c) 2013 Viet Trinh. All rights reserved.
//

#ifndef Ray_Tracer_Sphere_h
#define Ray_Tracer_Sphere_h

#include "Vector.h"

class Sphere{
public:
    Vector* center;
    float radius;
    Vector* materialColor_amb;
    Vector* materialColor_dif;
    Vector* materialColor_spec;
    
    Sphere();
    Sphere(Vector* c,float r);
    Sphere(float cx,float cy,float cz,float r);
    void setSphereColor(float aR,float aG,float aB,
                        float dR,float dG,float dB,
                        float sR,float sG,float sB);
};

#endif
