//
//  Triangle.h
//  Ray Tracer
//
//  Created by Viet Trinh on 4/13/13.
//  Copyright (c) 2013 Viet Trinh. All rights reserved.
//

#ifndef Ray_Tracer_Triangle_h
#define Ray_Tracer_Triangle_h

#include "Vector.h"

class Triangle{
public:
    Vector* v0;
    Vector* v1;
    Vector* v2;
    Vector* materialColor_amb;
    Vector* materialColor_dif;
    Vector* materialColor_spec;
    
    Triangle();
    Triangle(Vector* p0,Vector* p1,Vector* p2);
    Triangle(float v0x,float v0y,float v0z,
             float v1x,float v1y,float v1z,
             float v2x,float v2y,float v2z);
    
    void setTriangleColor(float aR,float aG,float aB,
                        float dR,float dG,float dB,
                        float sR,float sG,float sB);
    Vector findPlaneNormal();
    bool isPointInTriangle(Vector* R, Vector* n);
    
    
};

#endif
