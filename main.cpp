//
//  main.cpp
//  Ray Tracer
//
//  Created by Viet Trinh on 4/12/13.
//  Copyright (c) 2013 Viet Trinh. All rights reserved.
//

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#include <GLUI/glui.h>
#else
#include <GL/glut.h>
#include <GL/glui.h>
#endif

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <climits>
#include <unistd.h>
#include "Vector.h"
#include "Sphere.h"
#include "Triangle.h"

#define PI 3.14159265359
using namespace std;

/*----- GLOBAL VARIABLES -------------*/
//=== GLUI variables ===
int window;
GLUI* glui;
GLUI_RadioGroup* choose_light;

//=== GLUT variables ===
int button, state = 1;
float gx, gy;
int x_win = 512;
int z_win = 512;
float VIEWER[3]={100.0,400.0,100.0};
float LOOKAT[3]={static_cast<float>(x_win/2.0),0.0,static_cast<float>(z_win/2.0)};
float UPVECTOR[3]={0.0,1.0,0.0};
Vector view_dir;           // direction vector from viewer to look-at-point

//=== Scenic variables ===
/*--- camera & screen ---*/
float rayImage[512][512][3];
Vector* rightvec = new Vector(0,0,1);
Vector* upvec = new Vector(0,1,0);
Vector* camera = new Vector(-350.0,x_win/2,z_win/2);
Vector pixel;
Vector ray;

/*--- light sources ---*/
Vector* lightpos;
Vector* L1;
Vector* L2;
Vector* L3;
Vector* L4;
Vector* L5;
int lightSource = 0;

/*--- object color ---*/
Vector* lightColor_amb = new Vector(0.2, 0.2, 0.2);    // r, g, b
Vector* lightColor_dif= new Vector(0.8, 0.3, 0.1);     // r, g, b
Vector* lightColor_spec= new Vector(0.8, 0.3, 0.1);    // r, g, b

/*--- triangle vertices ---*/
Vector* vA;
Vector* vB;
Vector* vC;
Vector* vD;
Vector* v1;
Vector* v2;
Vector* v3;
Vector* v4;

/*--- objects in scene ---*/
Sphere* listSpheres[3];
Triangle* listTriangles[4];
Triangle* mirrors[2];
int numOfTriangles = 0;
int numOfSpheres = 0;

/*----- FUNCTIONS DECLARATION --------*/
void init();
void displayScreen();
void mouseClicks(int but,int sta,int x,int y);
void mouseMoves(int x, int y);
void keyPresses(unsigned char c, int x, int y);
void glui_cb(int control);

void setUpObjects();
void RayTracing(Vector* camera,Vector ray,float rayImage[]);
bool isIntersectSphere(Sphere* lsphere, Vector* lcamera,Vector lray, float* intersection);
bool isIntersectTriangle(Triangle* ltriangle, Vector* lcamera,Vector lray, float* intersection);
Vector implementPhongLighting(Vector* lightColor_amb, Vector* lightColor_dif,Vector* lightColor_spec,
                              Vector* materialColor_amb, Vector* materialColor_dif, Vector* materialColor_spec,
                              Vector* normal, Vector* lightpos,Vector* camera, Vector* Point, int index, bool isSphere);
bool isPointInShadow(Vector* P, int index, bool isSphere);
Vector isReflectedFromMirror(Vector* lcamera,Vector lray, bool* intersectMirror);

//===== main function ==================================
int main(int argc, char**argv){
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	
	// Create a window
	window = glutCreateWindow("Ray Tracer");
	glutPositionWindow(30, 30);
	glutReshapeWindow(x_win, z_win);
	
	// Program start here...
    setUpObjects();
    glutDisplayFunc(displayScreen);
    GLUI_Master.set_glutMouseFunc(mouseClicks);
    glutMotionFunc(mouseMoves);
    GLUI_Master.set_glutKeyboardFunc(keyPresses);
    
    glui = GLUI_Master.create_glui("",0,550,30);
    GLUI_Panel* lights_panel = glui->add_panel("",GLUI_PANEL_NONE);
    glui->add_statictext_to_panel(lights_panel, "Light Sources");
    choose_light = glui->add_radiogroup_to_panel(lights_panel,&lightSource,0,glui_cb);
    glui->add_radiobutton_to_group(choose_light, "Light 1 (near_left)");
    glui->add_radiobutton_to_group(choose_light, "Light 2 (far_left)");
    glui->add_radiobutton_to_group(choose_light, "Light 3 (near_right)");
    glui->add_radiobutton_to_group(choose_light, "Light 4 (far_right)");
    glui->add_radiobutton_to_group(choose_light, "Light 5 (center)");
    glui->add_statictext_to_panel(lights_panel, "");
    glui->add_button_to_panel(lights_panel,"Quit",5, (GLUI_Update_CB)exit)->set_alignment(GLUI_ALIGN_CENTER);
    
    glutMainLoop();
	return 0;
}

//===== displayScreen ===================================
void displayScreen(){
    
    // code here for drawing object...
    glClearColor(0.0,0.0,0.0,1.0);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    // Determine light source in use
    if      (lightSource == 0){lightpos = L1;}
    else if (lightSource == 1){lightpos = L2;}
    else if (lightSource == 2){lightpos = L3;}
    else if (lightSource == 3){lightpos = L4;}
    else                      {lightpos = L5;}
    
    // Implement ray tracing and calculate color for pixels
    for (int i=0; i<512; i++) {
        for (int j=0; j<512; j++) {
            Vector v1 = rightvec->multipleScalar((float)i);
            Vector v2 = upvec->multipleScalar((float)j);
            pixel = v1.add(&v2);
            ray = pixel.subtract(camera);
            ray.normalize();
            RayTracing(camera, ray, rayImage[j][i]);
        }
    }
    
    // Render scene from colored pixels
    glDrawPixels(x_win, z_win, GL_RGB, GL_FLOAT, rayImage);
    
    glutSwapBuffers();
}

//===== mouseClicks ====================================
void mouseClicks(int but,int sta,int x,int y){
    button = but;
    state = sta;
    
    
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
		
        gx = float(x)/x_win;
        gy = float(z_win-y)/z_win;
        
        // code here...
    }
    
    glutPostRedisplay();
    
}

//===== mouseMoves ====================================
void mouseMoves(int x, int y){
    
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
		gx = float(x)/x_win;
        gy = float(z_win-y)/z_win;
        
        // code here..
        
    }
    
    glutPostRedisplay();
    
}

//===== keyPresses ====================================
void keyPresses(unsigned char c, int x, int y){
    
    glutPostRedisplay();
}


//===== keyPresses ====================================
void glui_cb(int control){

    if (control == 0) {
        ;
    }
        
    if (glutGetWindow()!= window) {
        glutSetWindow(window);
    }
    glutPostRedisplay();
}

//=================================================================
void setUpObjects(){
    
    //----- SET UP SPHERE OBJECTS -----
    listSpheres[0] = new Sphere(500.0,84.0,356.0,70.0);
    listSpheres[0]->setSphereColor(0.54, 0.17, 0.89, 0.54, 0.17, 0.89, 0.94, 0.94, 0.94);
   
    listSpheres[1] = new Sphere(384.0,384.0,384.0,70.0);
    listSpheres[1]->setSphereColor(0.86, 0.47, 0.54, 0.86, 0.47, 0.54, 0.94, 0.94, 0.94);
   
    listSpheres[2] = new Sphere(500.0,84.0,600.0,70.0);
    listSpheres[2]->setSphereColor(0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.94, 0.94, 0.94);
    
    numOfSpheres = 3;
    
    //----- SET UP TRIANGLE OBJECTS -----
    vA = new Vector(250.0,256.0,0.0);
    vB = new Vector(270.0,400.0,0.0);
    vC = new Vector(150.0,256.0,170.0);
    vD = new Vector(240.0,500.0,125.0);
    
    listTriangles[0] = new Triangle(vA,vC,vD);
    listTriangles[0]->setTriangleColor(1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 0.94, 0.94, 0.94);
    
    listTriangles[1] = new Triangle(vA,vD,vB);
    listTriangles[1]->setTriangleColor(0.0, 0.0, 0.98, 0.0, 0.0, 0.98, 0.94, 0.94, 0.94);
    
    listTriangles[2] = new Triangle(2*x_win,0,0,0,0,2*z_win,2*x_win,0,2*z_win);
    listTriangles[2]->setTriangleColor(0.0, 0.0, 1.0, 0.0, 0.75, 1.0, 1.0, 1.0, 1.0);
    
    listTriangles[3] = new Triangle(2*x_win,0,0,0,0,-1*z_win,0,0,2*z_win);
    listTriangles[3]->setTriangleColor(0.0, 0.0, 1.0, 0.0, 0.75, 1.0, 1.0, 1.0, 1.0);
    
    numOfTriangles = 4;
    
    //----- SET UP MIRROR OBJECTS -----
    v1 = new Vector(2*x_win,0,0);
    v2 = new Vector(2*x_win,0,1.5*z_win);
    v3 = new Vector(2*x_win,1.5*x_win,0);
    v4 = new Vector(2*x_win,1.5*x_win,1.5*z_win);
    
    mirrors[0] = new Triangle(v1,v2,v3);
    mirrors[0]->setTriangleColor(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);
    
    mirrors[1] = new Triangle(v2,v4,v3);
    mirrors[1]->setTriangleColor(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);
    
    //----- SET UP LIGHT SOURCES -----
    L1 = new Vector(0.0, 600.0, 0.0);
    L2 = new Vector(512, 600.0, 0.0);
    L3 = new Vector(0.0, 600.0, 512.0);
    L4 = new Vector(512.0, 600.0, 512.0);
    L5 = new Vector(256, 600.0, 256.0);
    
}

//=================================================================
// Ray equation: X(t) = P +tD
// where P is camera location, and D is ray
// D is normalized
void RayTracing(Vector* camera,Vector ray,float rayImage[]){
    
    /*--- variables for calculting sphere intersection--- */
    float sphereIntersect_t = 0.0;
    float min_t_sphere = 0.0;
    bool isSphereIntersect = false;
    int index_sphere=-1;

    /*--- variables for calculting triangle intersection--- */
    float triangleIntersect_t = 0.0;
    float min_t_triangle=0.0;
    bool isTriangleIntersect =false;
    int index_triangle=-1;
    
    /*--- variables for calculting the closet intersection--- */    
    float min_t;
    bool isIntersect = false;
    bool isSphere = false;
    int index = -1;
    Vector RGB;
    Vector intersectionPoint;
    Vector norm;
    Vector* materialColor_amb;
    Vector* materialColor_dif;
    Vector* materialColor_spec;

    bool intersectMirror = false;
    
    /*------ Does this ray intersect any sphere? ----------------*/
    for (int i=0; i<numOfSpheres; i++) {
        bool intersectThisSphere = isIntersectSphere(listSpheres[i], camera, ray, &sphereIntersect_t);
        if (intersectThisSphere) {
            if (min_t_sphere < sphereIntersect_t) {
                min_t_sphere = sphereIntersect_t;
                index_sphere = i;
                isSphereIntersect = true;
            }
        }

    }
    
    /*------ Does this ray intersect any triangle? ----------------*/
    for (int i=0; i<numOfTriangles; i++) {
        bool intersectThisTriangle = isIntersectTriangle(listTriangles[i], camera, ray, &triangleIntersect_t);
        if (intersectThisTriangle) {
            if (min_t_triangle < triangleIntersect_t) {
                min_t_triangle = triangleIntersect_t;
                index_triangle = i;
                isTriangleIntersect = true;
            }
        }
    }
    
    /*------ Find the smallest t values for intersection point ----------------*/
    // if this ray intersects both a sphere and a triangle
    if (isSphereIntersect && isTriangleIntersect) {
        if (min_t_sphere <= min_t_triangle) {
            min_t = min_t_sphere;
            index = index_sphere;
            isSphere = true;
        }
        else{
            min_t = min_t_triangle;
            index = index_triangle;
            isSphere = false;
        }
        isIntersect = true;
    }
    // if this ray intersects a sphere only
    else if (isSphereIntersect && !isTriangleIntersect){
        min_t = min_t_sphere;
        index = index_sphere;
        isSphere = true;
        isIntersect = true;
    }
    // if this ray intersects a triangle only
    else if (!isSphereIntersect && isTriangleIntersect){
        min_t = min_t_triangle;
        index = index_triangle;
        isSphere = false;
        isIntersect = true;
    }
    // no intersection found
    else{
        /*------ Does this ray intersect any mirror? ----------------*/
        RGB = isReflectedFromMirror(camera, ray, &intersectMirror);
        if (intersectMirror) {
            isIntersect = true;
        }
        else{
            isIntersect = false;
        }
    }
    
    /*------ Calculating color for the intersection point ----------------*/
    if (isIntersect) {
        /* This ray does not intersect any mirror
           The pixel is the intersection point between this ray
           and any object in the scene
         */
        if(!intersectMirror){
            Vector tD = ray.multipleScalar(min_t);
            intersectionPoint = camera->add(&tD);
            
            // if the intersection is on a sphere
            if (isSphere) {
                norm = intersectionPoint.subtract(listSpheres[index]->center);
                norm.normalize();
                
                materialColor_amb = listSpheres[index]->materialColor_amb;
                materialColor_dif = listSpheres[index]->materialColor_dif;
                materialColor_spec= listSpheres[index]->materialColor_spec;
            }
            // if the intersection is in a triangle
            else{
                norm = listTriangles[index]->findPlaneNormal();
                norm.normalize();
                
                materialColor_amb = listTriangles[index]->materialColor_amb;
                materialColor_dif = listTriangles[index]->materialColor_dif;
                materialColor_spec= listTriangles[index]->materialColor_spec;
            }
            
            // using Phong lighting model to calculate color of a pixel
            RGB = implementPhongLighting(lightColor_amb, lightColor_dif, lightColor_spec,
                                                materialColor_amb, materialColor_dif,materialColor_spec,
                                                &norm, lightpos, camera, &intersectionPoint,index,isSphere);
            
        }
        /* - If the ray intersect with any object, the color is
           the calculated color from Phong Lighting
           - If the ray intersect with any mirror, the color is
           the return color from reflection
           - If the ray non-intersect, color is black
         */
        rayImage[0] = RGB.x;
        rayImage[1] = RGB.y;
        rayImage[2] = RGB.z;
        
    }
}

//=================================================================
bool isIntersectSphere(Sphere* lsphere, Vector* lcamera,Vector lray, float* intersection){
    
    Vector M = lcamera->subtract(lsphere->center);
    float delta = pow(lray.dotProduct(&M),2) - pow(M.length(),2) + pow(lsphere->radius, 2);
    
    // if no intersection found
    if (delta <0) {
        return false;
    }
    // if intersection points are found
    else{
        float DdotM = lray.dotProduct(&M);
        float t1 = (-1)*DdotM - sqrt(delta);
        float t2 = (-1)*DdotM + sqrt(delta);
        
        if (t1>=0 && t2>=0) {
            if (t1<t2) {
                *intersection = t1;
            }
            else{
                *intersection = t2;
            }
            return true;
        }
        else{
            return false;
        }
    }
    
}

//=================================================================
bool isIntersectTriangle(Triangle* ltriangle, Vector* lcamera,Vector lray, float* intersection){
    
    // find intersection between a ray and a plane containing a triangle
    Vector n = ltriangle->findPlaneNormal();
    float k = n.dotProduct(ltriangle->v0);
    float t = (k - n.dotProduct(lcamera))/(n.dotProduct(&lray));
    Vector tD = lray.multipleScalar(t);
    Vector R = lcamera->add(&tD);
    
    // check whether the intersection point is in a triangle
    bool isR_inTriangle = ltriangle->isPointInTriangle(&R, &n);
    if (isR_inTriangle) {
        *intersection = t;
        return true;
    }
    else{
        return false;
    }
}

//=================================================================
Vector implementPhongLighting(Vector* lightColor_amb, Vector* lightColor_dif,Vector* lightColor_spec,
                              Vector* materialColor_amb, Vector* materialColor_dif, Vector* materialColor_spec,
                              Vector* normal, Vector* lightpos,Vector* camera, Vector* Point, int index, bool isSphere){
    Vector ret;
    bool isInShadow = false;
    
    // check whether this pixel is occluded
    isInShadow = isPointInShadow(Point,index,isSphere);
    
    if (isInShadow) {
        ret.x = 0.4;
        ret.y = 0.4;
        ret.z = 0.4;
    }
    else{
        Vector L = lightpos->subtract(Point);
        L.normalize();
        
        Vector V = camera->subtract(Point);
        V.normalize();
        
        Vector H = L.add(&V);
        H.normalize();
        
        Vector Color_amb = lightColor_amb->multiply(materialColor_amb);
        Vector Color_dif = (lightColor_dif->multiply(materialColor_dif)).multipleScalar(normal->dotProduct(&L));
        Vector Color_spec = (lightColor_spec->multiply(materialColor_spec)).multipleScalar(pow(normal->dotProduct(&H), 3.0));
        
        ret = Color_amb.add(&Color_dif).add(&Color_spec);
    }
    return ret;
}

//=================================================================
bool isPointInShadow(Vector* P, int index, bool isSphere){
    bool isCastSphere = false;
    bool isCastTriangle = false;
    bool ret = false;
    float t;
    
    Vector D = lightpos->subtract(P);
    float tLength = D.length();
    D.normalize();
    
    /*------ Does this ray intersect any sphere? ----------------*/
    for (int i=0; i<numOfSpheres; i++) {
        // the checked pixel is not belong to this sphere listSpheres[i]
        if (!isSphere) {
            bool intersectThisSphere = isIntersectSphere(listSpheres[i], P, D, &t);
            if (intersectThisSphere) {
                if (t>0.0001 && t<tLength) {
                    isCastSphere = true;
                }
            }
        }
    }
    
    /*------ Does this ray intersect any triangle? ----------------*/
    for (int i=0; i<numOfTriangles; i++) {
        // the checked pixel is not belong to this triangle listTriangle[i]
        if (i!=index && !isSphere) {
            bool intersectThisTriangle = isIntersectTriangle(listTriangles[i], P, D, &t);
            if (intersectThisTriangle) {
                if (t>0.0001 && t<tLength) {
                    isCastTriangle = true;
                }
            }
        }
    }

    if (isCastSphere || isCastTriangle) {
        ret = true;
    }
    
    return ret;
}

//=================================================================
Vector isReflectedFromMirror(Vector* lcamera,Vector lray, bool* intersectMirror){
    
    Vector ret;
    float t = 0.0;
    float min_t= 0.0;
    int index =-1;
    float color[]={0.0,0.0,0.0};
    bool isIntersectMirror = false;
    
    Vector Color_nr;
    Vector* materialColor_amb;
    Vector* materialColor_dif;
    Vector* materialColor_spec;
    
    /*------ Does this ray intersect any mirror? ----------------*/
    for (int i=0; i<2; i++) {
        bool intersectThisMirror = isIntersectTriangle(mirrors[i], lcamera, lray, &t);
        if (intersectThisMirror) {
            if (min_t < t) {
                min_t = t;
                index = i;
                isIntersectMirror = true;
                *intersectMirror = isIntersectMirror;
            }
        }
    }
    
    /*------ This ray intersects a mirror at point P ----------------*/
    /*       Calculated the reflected ray R for this mirror          */
    if (isIntersectMirror) {
        Vector tD = lray.multipleScalar(min_t);
        Vector P = lcamera->add(&tD);
        
        Vector V = lcamera->subtract(&P);
        V.normalize();
        
        Vector N = mirrors[index]->findPlaneNormal();
        N.normalize();
        
        Vector R = N.multipleScalar(2*N.dotProduct(&V)).subtract(&V);
        R.normalize();
        
        /* Does this reflected ray R intersect any object? */
        RayTracing(&P, R, color);
        
        /* Calculate the color of the intersection point for the mirror */
        materialColor_amb = mirrors[index]->materialColor_amb;
        materialColor_dif = mirrors[index]->materialColor_dif;
        materialColor_spec= mirrors[index]->materialColor_spec;
        Color_nr = implementPhongLighting(lightColor_amb, lightColor_dif, lightColor_spec, materialColor_amb, materialColor_dif, materialColor_spec, &N, lightpos, camera, &P, index, false);
    }

    
    /* If the reflected ray intersect any object
       the color of this pixel is the color the reflected
       ray sees
     */
    if (color[0]>0 || color[1]>0 || color[2]>0) {
        ret.x = color[0];
        ret.y = color[1];
        ret.z = color[2];
        
    }
    /* if the reflected ray intersect no object
       the color of this pixel is the color of the mirror
       finalColor = f*Color_reflect + (1-f)*Color_non_reflect
     */
    else{
        ret.x = 0.6*Color_nr.x + 0.4*1.0;
        ret.y = 0.6*Color_nr.y + 0.4*1.0;
        ret.z = 0.6*Color_nr.z + 0.4*1.0;
    }
    
    return ret;
}

