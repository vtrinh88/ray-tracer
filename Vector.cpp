//
//  Vector.cpp
//  Ray Tracer
//
//  Created by Viet Trinh on 4/12/13.
//  Copyright (c) 2013 Viet Trinh. All rights reserved.
//

#include "Vector.h"
#include <cmath>

Vector Vector::add(Vector *v){
    Vector ret;

    ret.x = x + v->x;
    ret.y = y + v->y;
    ret.z = z + v->z;
    
    return ret;
}

Vector Vector::subtract(Vector *v){
    Vector ret;
    
    ret.x = x - v->x;
    ret.y = y - v->y;
    ret.z = z - v->z;
    
    return ret;
}

Vector Vector::multiply(Vector* v){
    Vector ret;
    
    ret.x = x*v->x;
    ret.y = y*v->y;
    ret.z = z*v->z;
    
    return ret;
}

Vector Vector::crossProduct(Vector *v){
    Vector ret;
    
    ret.x = y*v->z - v->y*z;
    ret.y = z*v->x - v->z*x;
    ret.z = x*v->y - v->x*y;
    
    return ret;
}

float Vector::dotProduct(Vector *v){
    float ret = x*v->x + y*v->y + z*v->z;
    return ret;
}

Vector Vector::multipleScalar(float coef){
    Vector ret;
    ret.x = x*coef;
    ret.y = y*coef;
    ret.z = z*coef;
    return ret;
}

void Vector::normalize(){
    float d = sqrt(x*x + y*y + z*z);
    x = x/d;
    y = y/d;
    z = z/d;
}

float Vector::length(){
    float ret = sqrt(x*x + y*y + z*z);
    return ret;
}

